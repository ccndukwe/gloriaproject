package com.gloria.project;

import java.util.ArrayList;
import java.util.List;

public class Product {

    private String name;
    private int totalReview;
    private int minedReviewCount;
    private double rating;
    private double price;
    private String reviewLink;
    private int salesRank;
    private List<Review> reviewList = new ArrayList<>();

    public String getName() {
        return name;
    }

    public int getMinedReviewCount() {
        return minedReviewCount;
    }

    public void setMinedReviewCount(int minedReviewCount) {
        this.minedReviewCount = minedReviewCount;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalReview() {
        return totalReview;
    }

    public void setTotalReview(int totalReview) {
        this.totalReview = totalReview;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getReviewLink() {
        return reviewLink;
    }

    public void setReviewLink(String reviewLink) {
        this.reviewLink = reviewLink;
    }

    public int getSalesRank() {
        return salesRank;
    }

    public void setSalesRank(int salesRank) {
        this.salesRank = salesRank;
    }

    public List<Review> getReviewList() {
        return reviewList;
    }

    public void setReviewList(List<Review> reviewList) {
        this.reviewList = reviewList;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", totalReview=" + totalReview +
                ", rating=" + rating +
                ", price=" + price +
                ", reviewLink='" + reviewLink + '\'' +
                ", salesRank=" + salesRank +
                ", reviewList=" + reviewList +
                '}';
    }
}
