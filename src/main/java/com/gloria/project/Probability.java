package com.gloria.project;

import java.util.ArrayList;
import java.util.List;

public class Probability {

    private double neg = 0.00;
    private double neutral = 0.00;
    private double pos = 0.00;


    public double getNeg() {
        return neg;
    }

    public void setNeg(double neg) {
        this.neg = neg;
    }

    public double getNeutral() {
        return neutral;
    }

    public void setNeutral(double neutral) {
        this.neutral = neutral;
    }

    public double getPos() {
        return pos;
    }

    public void setPos(double pos) {
        this.pos = pos;
    }


    @Override
    public String toString() {
        return "Probability{" +
                "neg=" + neg +
                ", neutral=" + neutral +
                ", pos=" + pos +
                '}';
    }
}
