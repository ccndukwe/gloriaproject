package com.gloria.project;

import java.util.ArrayList;
import java.util.List;

public class Review {

    private String subject= "";
    private String body="";
    private double rating = 0.00;
    private Sentiment sentiment = new Sentiment();
    private List<String> wordList = new ArrayList<>();

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public Sentiment getSentiment() {
        return sentiment;
    }

    public void setSentiment(Sentiment sentiment) {
        this.sentiment = sentiment;
    }

    public List<String> getWordList() {
        return wordList;
    }

    public void setWordList(List<String> wordList) {
        this.wordList = wordList;
    }

    @Override
    public String toString() {
        return "Review{" +
                "subject='" + subject + '\'' +
                ", body='" + body + '\'' +
                ", rating=" + rating +
                ", sentiment=" + sentiment +
                ", wordList=" + wordList +
                '}';
    }
}

