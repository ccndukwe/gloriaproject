package com.gloria.project;

import java.util.ArrayList;
import java.util.List;

public class Sentiment {

    private Probability probability = new Probability();
    private String label ="";


    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Probability getProbability() {
        return probability;
    }

    public void setProbability(Probability probability) {
        this.probability = probability;
    }

    @Override
    public String toString() {
        return "Sentiment{" +
                "probability=" + probability +
                ", label='" + label + '\'' +
                '}';
    }
}
