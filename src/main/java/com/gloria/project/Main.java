package com.gloria.project;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jackson.map.ObjectMapper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main extends Application {

    private final String BASE_URL = "https://www.amazon.com";
    private WebView webView;
    private ObjectMapper mapper;
    private WebEngine webEngine;
    private int level = 0;
    private Product product;
    private Review review;
    private List<Review> reviewList = new ArrayList<>();
    private List<Product> productList = new ArrayList<>();
    private Text productFileName;
    private Text wordListFileName;
    private Text countText;
    private TextField maxNumberOfReviewsField;
    private int maxNumberOfReviews;
    private VBox vBox;
    private VBox topVBox;
    private boolean filesSelected = false;
    private String appFolder;
    //    private HBox hBox;
    private Button productListSelectButton;
    private Button wordListSelectButton;
    private File excelFile;
    private File jsonFile;
    private BorderPane mainBorderPane, centerBorderPane;
    private StackPane webViewPane;
    private Button mineButton;
    private Button excelButton;
    private List<String> wordList;
    private Image analysisImage;
    private ImageView imageView;
    private Button jsonButton;
    private ProgressIndicator progressIndicator;
    private ProgressBar progressBar;
    private ProgressBar webViewProgressBar;
    private Text processingText;
    private FileChooser fileChooser;
    private File urlFile;
    private File wordListFile;
    private List<String> productUrls;
    private RestClient restClient;
    private String userHome;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        StackPane root = new StackPane();
        restClient = new RestClient();
        userHome = System.getProperty("user.home");
        appFolder = userHome + "/Desktop/AmazonMiner/Result/";


        wordList = new ArrayList<>();
        analysisImage = new Image("/analysis.gif", true);
        imageView = new ImageView();
        imageView.setImage(analysisImage);
        webView = new WebView();
        webEngine = webView.getEngine();
        webView.setDisable(true);

//        hBox = new HBox();
        vBox = new VBox();
        topVBox = new VBox();

        vBox.setMinWidth(400);
        vBox.setPadding(new Insets(100, 10, 10, 10));
        vBox.setAlignment(Pos.BASELINE_CENTER);
        vBox.setSpacing(20);

        topVBox.setPadding(new Insets(10, 10, 20, 10));
        topVBox.setAlignment(Pos.BASELINE_CENTER);
        topVBox.setSpacing(20);

//        hBox.setPadding(new Insets(20, 10, 10, 10));
//        hBox.setAlignment(Pos.BASELINE_CENTER);
//        hBox.setSpacing(20);


        Text school = new Text("Federal University of Technology, Owerri");
        school.setFill(Color.BLUE);
        school.setStyle("-fx-font-size: 20");
        Text title = new Text("IMPACT ANALYSIS OF SENTIMENT ON PRODUCT SALES");
        Text subTitle = new Text("Amazon Product Data Mining & Review Analysis");
        subTitle.setFill(Color.RED);
        Text maxReviewText = new Text("Enter max number of reviews per product");
        maxNumberOfReviewsField = new TextField();
        maxNumberOfReviewsField.setPrefColumnCount(4);
        maxNumberOfReviewsField.setMaxWidth(50);


        productFileName = new Text("");
        wordListFileName = new Text("");
        productFileName.setFill(Color.GREEN);
        wordListFileName.setFill(Color.GREEN);
        progressIndicator = new ProgressIndicator();
        progressBar = new ProgressBar();
        webViewProgressBar = new ProgressBar();
        webViewProgressBar.setVisible(false);

        progressBar.setVisible(false);

        excelButton = new Button("Open result as excel");
        jsonButton = new Button("Open result as json");
        productListSelectButton = new Button("Select products list file");
        wordListSelectButton = new Button("Select word list file");
        mineButton = new Button("Processs");
        mineButton.setStyle("-fx-text-fill: red");
        mineButton.setDisable(true);

        excelButton.setVisible(false);
        jsonButton.setVisible(false);

        progressIndicator.setVisible(false);

        processingText = new Text("Processing data mining...");
        countText = new Text("0");
        processingText.setStyle("-fx-font-size: 16");
        processingText.setStyle("-fx-text-alignment: center");
        processingText.setFill(Color.RED);
        processingText.setVisible(false);

        Text emptySpace1 = new Text("");
        Text emptySpace2 = new Text("");
        Text emptySpace3 = new Text("");
        Text emptySpace4 = new Text("");
        Text emptySpace5 = new Text("");


        vBox.getChildren().addAll(subTitle, emptySpace1, productFileName,
                productListSelectButton, wordListFileName, wordListSelectButton, emptySpace2,maxReviewText, maxNumberOfReviewsField, emptySpace3, mineButton,
                emptySpace4, progressIndicator, processingText, countText,excelButton, jsonButton, progressBar);

        topVBox.getChildren().addAll(school, title);

        mainBorderPane = new BorderPane();
        centerBorderPane = new BorderPane();
        webViewPane = new StackPane();

        centerBorderPane.setStyle("-fx-background-color: white");
        topVBox.setStyle("-fx-border-color: white white blue white");

        webViewPane.getChildren().addAll(webView, webViewProgressBar);
        centerBorderPane.setTop(topVBox);
        centerBorderPane.setCenter(webViewPane);
        mainBorderPane.setRight(vBox);
        mainBorderPane.setCenter(centerBorderPane);

        root.getChildren().addAll(mainBorderPane);

        Scene scene = new Scene(root);
        primaryStage.setTitle("Amazon Data Miner & Review Analysis");
        primaryStage.setScene(scene);
        primaryStage.setMaximized(true);
        primaryStage.show();

        productListSelectButton.setOnAction((ActionEvent e) -> {

            fileChooser = new FileChooser();

            if (!productFileName.getText().isEmpty()) {
                mineButton.setDisable(true);
            }

            // Set extension filter
            FileChooser.ExtensionFilter extFilter =
                    new FileChooser.ExtensionFilter("TEXT files (*.txt)", "*.txt");
            fileChooser.getExtensionFilters().add(extFilter);
            urlFile = fileChooser.showOpenDialog(primaryStage);

            if (urlFile != null) {
                productFileName.setText(urlFile.getAbsolutePath());
                if(filesSelected){
                    mineButton.setDisable(false);
                }
                else {
                    filesSelected = true;
                }
            }
        });

        wordListSelectButton.setOnAction((ActionEvent e) -> {

            fileChooser = new FileChooser();

            if (!wordListFileName.getText().isEmpty()) {
                mineButton.setDisable(true);
            }

            // Set extension filter
            FileChooser.ExtensionFilter extFilter =
                    new FileChooser.ExtensionFilter("TEXT files (*.txt)", "*.txt");
            fileChooser.getExtensionFilters().add(extFilter);
            wordListFile = fileChooser.showOpenDialog(primaryStage);

            if (wordListFile != null) {
                wordListFileName.setText(wordListFile.getAbsolutePath());
                if(filesSelected){
                    mineButton.setDisable(false);
                }
                else {
                    filesSelected = true;
                }
            }
        });

        excelButton.setOnAction((ActionEvent e) -> {
            try {

                if (Desktop.isDesktopSupported()) {
                    Desktop.getDesktop().open(excelFile);
                }


            } catch (Exception e1) {
                e1.printStackTrace();
            }
        });
        excelButton.setOnAction((ActionEvent e) -> {
            try {

                if (Desktop.isDesktopSupported()) {
                    Desktop.getDesktop().open(excelFile);
                }


            } catch (Exception e1) {
                e1.printStackTrace();
            }
        });

        jsonButton.setOnAction((ActionEvent e) -> {

            try {

                if (Desktop.isDesktopSupported()) {
                    Desktop.getDesktop().open(jsonFile);
                }


            } catch (IOException e1) {
                e1.printStackTrace();
            }

        });


        mineButton.setOnAction((ActionEvent e) -> {
            try {
                countText.setText("0");
                processingText.setText("Data mining Processing...");
                productUrls = new ArrayList<>();
                Scanner sc = new Scanner(urlFile);
                while (sc.hasNextLine()) {
                    productUrls.add(sc.nextLine());
                }
                level = 0;
                if (!productUrls.isEmpty()) {
                    try {
                        maxNumberOfReviews = Integer.parseInt(maxNumberOfReviewsField.getText());
                    } catch (Exception ex) {
                    }
                    if (wordListFile.exists()) {
                        wordList = new ArrayList<>();
                        sc = new Scanner(wordListFile);
                        while (sc.hasNextLine()) {
                            wordList.add(sc.nextLine());
                        }
                        System.out.println(wordList);
                    }


                    mineButton.setDisable(true);
                    productListSelectButton.setDisable(true);

                    excelButton.setDisable(true);
                    jsonButton.setDisable(true);

                    processingText.setVisible(true);

                    productList = new ArrayList<>();
                    progressIndicator.setVisible(true);
                    centerBorderPane.setCenter(webViewPane);
                    if (!mineData.isRunning()) {
                        webEngine.load(productUrls.get(0));
                        mineData.reset();
                        mineData.start();
                    }
                }
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
        });

        webEngine.getLoadWorker().stateProperty().addListener(
                new ChangeListener<Worker.State>() {
                    public void changed(ObservableValue ov, Worker.State oldState, Worker.State newState) {

                        if (newState == Worker.State.RUNNING) {
                            webViewProgressBar.setVisible(true);
                        }
                        if (newState == Worker.State.FAILED) {
                            webViewProgressBar.setVisible(false);
                        }
                        if (newState == Worker.State.SUCCEEDED) {
                            webViewProgressBar.setVisible(false);
                        }
                    }
                });
    }

    Service analizeReview = new Service() {
        @Override
        protected Task createTask() {
            return new Task() {
                @Override
                protected Void call() {

                    int k=0;
                    for (Product product : productList) {
                        k++;
int l= 0;
                        for (Review review : product.getReviewList()) {
                            l++;
                            List<String> reviewWordList = new ArrayList<>();
                            try {
                                String result = restClient.postRequest(review.getBody());
                                if (!result.equals("ERR")) {
                                    mapper = new ObjectMapper();
                                    Sentiment sentiment = mapper.readValue(result, Sentiment.class);
                                    review.setSentiment(sentiment);
                                } else {
                                    result = restClient.postRequest(review.getBody());
                                    if (!result.equals("ERR")) {
                                        mapper = new ObjectMapper();
                                        Sentiment sentiment = mapper.readValue(result, Sentiment.class);
                                        review.setSentiment(sentiment);
                                    }
                                    else {
                                        System.out.println("Error");
                                    }
                                }
                                for (String word : wordList) {
                                    if (review.getBody().toLowerCase().contains(word.toLowerCase())) {
                                        reviewWordList.add(word.toLowerCase());
                                    }
                                }
                                review.setWordList(reviewWordList);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            int finalL = l;
                            int finalK = k;
                            Platform.runLater(() -> {
                                countText.setText(finalL +" of "+ finalK);
                            });
                        }


                    }
                    exportAsExcel();
                    exportAsJson();
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    Platform.runLater(() -> {
                        analysisImage = new Image("/analysis.jpg");
                        imageView.setImage(analysisImage);
                        processingText.setText("Successfully Completed");
                        progressIndicator.setVisible(false);
                        productListSelectButton.setDisable(false);
                        mineButton.setDisable(false);

                        excelButton.setVisible(true);
                        jsonButton.setVisible(true);

                        excelButton.setDisable(false);
                        jsonButton.setDisable(false);
                    });

                    return null;
                }
            };

        }
    };

    public void exportAsExcel() {
        try {

            excelFile = new File(appFolder + System.currentTimeMillis() + ".xlsx");
            excelFile.getParentFile().mkdirs();

            if (!excelFile.exists()) {
                excelFile.createNewFile();
            }

            Workbook workbook = new XSSFWorkbook();
            for (Product product : productList) {
                System.out.println(product.getName());
                Sheet sheet;
                try {
                    sheet = workbook.createSheet(product.getName());
                }
                catch (Exception e){
                     sheet = workbook.createSheet();
                }


                Row row = sheet.createRow(0);
                Cell cell = row.createCell(0);
                cell.setCellValue("Product Name: ");
                cell = row.createCell(1);
                cell.setCellValue(product.getName());

                row = sheet.createRow(1);
                cell = row.createCell(0);
                cell.setCellValue("Rating: ");
                cell = row.createCell(1);
                cell.setCellValue(product.getRating());

                row = sheet.createRow(2);
                cell = row.createCell(0);
                cell.setCellValue("Price: ");
                cell = row.createCell(1);
                cell.setCellValue(product.getPrice());

                row = sheet.createRow(3);
                cell = row.createCell(0);
                cell.setCellValue("Total Review: ");
                cell = row.createCell(1);
                cell.setCellValue(product.getTotalReview());

                row = sheet.createRow(4);
                cell = row.createCell(0);
                cell.setCellValue("Mined Review: ");
                cell = row.createCell(1);
                cell.setCellValue(product.getMinedReviewCount());

                row = sheet.createRow(5);
                cell = row.createCell(0);
                cell.setCellValue("Sales Rank: ");
                cell = row.createCell(1);
                cell.setCellValue(product.getSalesRank());

                row = sheet.createRow(7);
                cell = row.createCell(0);
                cell.setCellValue("REVIEW SUBJECT");
                cell = row.createCell(1);
                cell.setCellValue("REVIEW RATING");
                cell = row.createCell(2);
                cell.setCellValue("REVIEW BODY");
                cell = row.createCell(3);
                cell.setCellValue("SENTIMENT");
                cell = row.createCell(4);
                cell.setCellValue("POSITIVE SENTIMENT POINT");
                cell = row.createCell(5);
                cell.setCellValue("NEGATIVE SENTIMENT POINT");
                cell = row.createCell(6);
                cell.setCellValue("NEUTRAL SENTIMENT POINT");

                for (int i = 0; i < wordList.size(); i++) {
                    cell = row.createCell(7 + i);
                    cell.setCellValue(wordList.get(i).toUpperCase());
                }


                for (int i = 0; i < product.getReviewList().size(); i++) {
                    Review review = product.getReviewList().get(i);

                    row = sheet.createRow(i + 8);
                    cell = row.createCell(0);
                    cell.setCellValue(review.getSubject());
                    cell = row.createCell(1);
                    cell.setCellValue(review.getRating());
                    cell = row.createCell(2);
                    cell.setCellValue(review.getBody());
                    cell = row.createCell(3);
                    cell.setCellValue(review.getSentiment().getLabel());
                    cell = row.createCell(4);
                    cell.setCellValue(review.getSentiment().getProbability().getPos());
                    cell = row.createCell(5);
                    cell.setCellValue(review.getSentiment().getProbability().getNeg());
                    cell = row.createCell(6);
                    cell.setCellValue(review.getSentiment().getProbability().getNeutral());

                    for (int j = 0; j < wordList.size(); j++) {
                        cell = row.createCell(7 + j);
                        if (review.getWordList().contains(wordList.get(j).toLowerCase())) {
                            cell.setCellValue(wordList.get(j).toLowerCase());
                        }
                    }
                }


            }


            FileOutputStream fileOut = new FileOutputStream(excelFile);
            workbook.write(fileOut);
            fileOut.close();

        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }


    public void exportAsJson() {
        try {

            jsonFile = new File(appFolder + System.currentTimeMillis() + ".json");
            jsonFile.getParentFile().mkdirs();

            if (!jsonFile.exists()) {
                jsonFile.createNewFile();
            }
            mapper = new ObjectMapper();
            mapper.writeValue(jsonFile, productList);

        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    Service mineData = new Service() {
        @Override
        protected Task createTask() {
            return new Task() {
                @Override
                protected Void call() {

                   for (int i = 0; i < productUrls.size(); i++) {
                        try {

                            Document document = Jsoup.connect(productUrls.get(i))
                                    .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.38 Safari/537.36")
                                    .maxBodySize(0)
                                    .timeout(60000)
                                    .get();
                            product = new Product();
                            reviewList = new ArrayList<>();
                            product.setName(document.getElementById("productTitle").text());
                            String numReview = document.getElementById("acrCustomerReviewText").text().replace(",", "");
                            product.setTotalReview(Integer.parseInt(numReview.substring(0, numReview.indexOf(" "))));
                            String ratingText = document.getElementById("acrPopover").text();
                            product.setRating(Double.parseDouble(ratingText.substring(0, ratingText.indexOf(" "))));
                            product.setPrice(Double.parseDouble(document.getElementById("priceblock_ourprice").text().replace("$", "").replace(",", "")));
                            product.setReviewLink(document.getElementById("dp-summary-see-all-reviews").attr("href"));

                            Element docElement = document.getElementsContainingText("Best Sellers Rank #").last();

                            int a = docElement.text().indexOf("#");
                            int b = docElement.text().indexOf("(");
                            String rankText = docElement.text().substring(a, b).replace(",", "");
                            product.setSalesRank(Integer.parseInt(rankText.substring(1, rankText.indexOf(" "))));

                            product.setMinedReviewCount(product.getTotalReview());

                            if (maxNumberOfReviews <= 0 || maxNumberOfReviews > product.getMinedReviewCount()) {
                                maxNumberOfReviews = product.getMinedReviewCount();
                            } else {
                                product.setMinedReviewCount(maxNumberOfReviews);
                            }

                            document = Jsoup.connect(BASE_URL + product.getReviewLink())
                                    .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.38 Safari/537.36")
                                    .maxBodySize(0)
                                    .timeout(60000)
                                    .get();

                            int finalI = i;
                            Platform.runLater(() -> {
                                webEngine.load(BASE_URL + product.getReviewLink());
                                   countText.setText((finalI+1)+"");
                            });
                            int j = 0, page = 0;
                            while (j < maxNumberOfReviews) {
                                document.getElementsByClass("a-section").forEach(element -> {
                                    if (element.hasClass("review")) {
                                        review = new Review();
                                        String reviewText = element.getElementsByClass("a-link-normal").first().text();
                                        review.setRating(Double.parseDouble(reviewText.substring(0, reviewText.indexOf(" "))));
                                        review.setSubject(element.getElementsByTag("a").get(1).text());
                                        review.setBody(element.getElementsByClass("review-text").first().text());
                                        reviewList.add(review);
                                    }

                                });
                                page++;
                               document = Jsoup.connect(BASE_URL + product.getReviewLink() + "&pageNumber=" + (page + 1))
                                       .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.38 Safari/537.36")
                                       .maxBodySize(0)
                                       .timeout(60000)
                                       .get();
                                int finalPage = page;
                                Platform.runLater(() -> {
                                    webEngine.load(BASE_URL + product.getReviewLink() + "&pageNumber=" + (finalPage + 1));
                                });

                                j = page * 10;
                                int finalI1 = i;
                                int finalJ = j;
                                if(j<=maxNumberOfReviews) {
                                    Platform.runLater(() -> {
                                        countText.setText(finalJ + " of " + (finalI1 + 1));
                                    });
                                }

                            }
                            product.setReviewList(reviewList);
                            product.setMinedReviewCount(reviewList.size());
                            productList.add(product);

                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                    Platform.runLater(() -> {
                        processingText.setText("Data Mining Successfull\n Reviews analysis processing...\n");
                        analysisImage = new Image("/analysis.gif");
                        imageView.setImage(analysisImage);
                        centerBorderPane.setCenter(imageView);
                    });

                    if (productList.size() > 0 && productList.get(0).getReviewList().size() > 0) {
                        if (!analizeReview.isRunning()) {
                            analizeReview.reset();
                            analizeReview.start();
                        }
                    } else {
                        Platform.runLater(() -> {
                            analysisImage = new Image("/error.jpg");
                            imageView.setImage(analysisImage);
                            processingText.setText("Network issues or bad link");
                            progressIndicator.setVisible(false);
                            webViewProgressBar.setVisible(false);
                            productListSelectButton.setDisable(false);
                            mineButton.setDisable(false);
                        });

                    }
                    return null;
                }
            };
        }
    };
}
